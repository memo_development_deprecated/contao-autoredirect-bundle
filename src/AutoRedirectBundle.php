<?php
/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\AutoRedirectBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AutoRedirectBundle extends Bundle
{
}
