<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\AutoRedirectBundle\EventListener\DataContainer;

use Contao\CoreBundle\ServiceAnnotation\Callback;
use Contao\DataContainer;
use Contao\Message;
use Contao\Model;
use Contao\PageModel;
use Memo\AutoRedirectBundle\Model\Terminal42RedirectModel;

class CallbackListener
{

	/**
	 * @Callback(table="tl_page", target="config.onsubmit")
	 */
	public function onSubmitCallback(DataContainer $dc)
	{

		// Get the Page
		$intPageID = $dc->activeRecord->id;
		$objPage = PageModel::findById($intPageID);

		// Get the new values (from the Form)
		$strAliasForm = $dc->activeRecord->alias;
		$strOldAliasForm = $dc->activeRecord->alias_old;
		$arrOldAliasForm = explode(',', str_replace(' ', '', $strOldAliasForm));
		asort($arrOldAliasForm);

		// Get the old values (from the DB)
		$strAliasDB = $objPage->alias;
		$strOldAliasDB = $objPage->alias_old;
		$arrOldAliasDB = explode(',', str_replace(' ', '', $strOldAliasDB));
		asort($arrOldAliasDB);

		// Get all Redirects for this page and all redirects
		$colRedirect = Terminal42RedirectModel::findBy(array("page_id=?"), array($intPageID));
		$colAllRedirect = Terminal42RedirectModel::findAll();

		// Check if the page-alias is in a redirect - in that case, delete the redirect
		self::removeRedirectsByPage($objPage, $strAliasForm);

		// Check all cases and act as needed

			// Case #1 - no changes
			if($strAliasForm == $strAliasDB && $arrOldAliasForm == $arrOldAliasDB) {
				return true;
			}

			// Case #2 - old aliases were deleted
			if(is_array($arrOldAliasForm) && count($arrOldAliasForm) == 1 && $arrOldAliasForm[0] == '' && $strOldAliasDB != '') {

				// Delete all redirects from this page
				self::deleteRedirects($intPageID);

				// Delete the routing cache (so the old redirects disappear)
				self::clearCache();

				Message::addInfo("Alle automatischen Weiterleitungen für diese Seite wurden gelöscht");

				return true;
			}

			// Case #3 - manuel definition of redirects (no auto redirect added)
			if($arrOldAliasForm != $arrOldAliasDB){

				// Generate the new redirects
				self::generateRedirects($arrOldAliasForm, $strAliasForm, $objPage);

				return true;
			}

			// Case #4 - automatic alias change detection
			if($strAliasForm != $strAliasDB){

				// Add the old Alias to the list of old Aliases
				$arrOldAliasForm[] = $strAliasDB;

				// Generate the new redirects
				self::generateRedirects($arrOldAliasForm, $strAliasForm, $objPage);

				return true;
			}
	}

	public function deleteRedirects(int $intPageID, bool $bolClearCache=false)
	{
		$colRedirect = Terminal42RedirectModel::findBy(array("page_id=?"), array($intPageID));

		// Check if there is anything to be removed
		if($colRedirect){

			// Loop all redirects for this page
			foreach($colRedirect as $objRedirect){

				// Delete the redirects
				$objRedirect->delete();
			}

			// If wished - delete the routing cache
			if($bolClearCache){
				self::clearCache();
			}
		}
	}

	public function generateRedirects(array $arrOldAlias, string $strNewAlias, PageModel $objPage)
	{
		// Get the mode
		$strMode = \Config::get('redirectMode') ?: 'crawled';

		// Resepct the defined mode
		if($strMode == 'none' || ($strMode == 'crawled' && !$objPage->crawled)){
			return true;
		}

		// Delete the current redirects
		self::deleteRedirects($objPage->id);

		// Remove empty entriesand duplicates from the array and resrot
		$arrOldAlias = array_filter($arrOldAlias);
		$arrOldAlias = array_unique($arrOldAlias);
		asort($arrOldAlias);

		// Get Parameters for the redirect rules
		$strHost = $objPage->domain;
		$strLanguage = $objPage->rootLanguage;
		$bolPrependLocale = \System::getContainer()->getParameter('prepend_locale');
		$strSuffix = \System::getContainer()->getParameter('contao.url_suffix');

		// Get all Pages inside the root-page
		$arrPages = array();
		$objRootPage = PageModel::findById($objPage->rootId);
		$arrPages [$objRootPage->id] = $objRootPage->alias;
		if($colPages = PageModel::findAll()){

			foreach($colPages as $objSinglePage){

				// Remove all root-pages
				if($objSinglePage->type == 'root'){
					continue;
				}

				// Remove all pages  not from this tree
				if(!array_key_exists($objSinglePage->pid, $arrPages)){
					continue;
				}

				// Add the new alias to the array
				$arrPages [$objSinglePage->id] = $objSinglePage->alias;

				// Add the language to the new page (if needed)
				if($bolPrependLocale && $strLanguage != ''){
					$arrPages[$objSinglePage->id] = $strLanguage . '/' . $arrPages[$objSinglePage->id];
				}

				// Add the language to the new page (if needed)
				if($strSuffix){
					$arrPages[$objSinglePage->id] = $arrPages[$objSinglePage->id] . $strSuffix;
				}
			}
		}

		// Check if the new alias is the index alias (that disappears) and otherwise add the suffix
		if($strNewAlias == 'index'){
			$strNewAlias = '/';
		} else {
			$strNewAlias = '/' . $strNewAlias . $strSuffix;
		}

		// Check if a language should be added (only once
		if($bolPrependLocale){
			$strNewAlias = $strLanguage . $strNewAlias;
		}

		foreach($arrOldAlias as $key => $strOldAlias){

			// Check if the old alias is the index alias (that disappears) and otherwise add the suffix
			if($strOldAlias == 'index'){
				$strOldAlias = '/';
			} else {
				$strOldAlias = '/' . $strOldAlias . $strSuffix;
			}

			// Check if a language should be added
			if($bolPrependLocale){
				$strOldAlias = $strLanguage . $strOldAlias;
			}

			// Check that the old and new urls are not the same
			if($strNewAlias == $strOldAlias) {

				// Same alias this page detected -> remove from list
				unset($arrOldAlias[$key]);
				continue;
			}

			// Find Aliases of other pages
			if(in_array($strOldAlias, $arrPages)){

				// Same alias as other page detected -> remove from list
				unset($arrOldAlias[$key]);
				continue;
			}

			// Generate the new redirect
			$objRedirect = new Terminal42RedirectModel();
			$objRedirect->name = "Automatische Regel für Seite: " . $objPage->title;
			$objRedirect->type = "basic";
			$objRedirect->inactive = 0;
			$objRedirect->requestHosts = serialize(array($strHost));
			$objRedirect->requestPath = $strOldAlias;
			$objRedirect->responseCode = \Config::get('redirectType') ?: 301;
			$objRedirect->responseUri = '/' . $strNewAlias;
			$objRedirect->tstamp = time();
			$objRedirect->requestRequirements = 'a:0:{}';
			$objRedirect->generated_by_memo = 1;
			$objRedirect->page_id = $objPage->id;
			$objRedirect->save();

			//Post Message in Backend
			Message::addConfirmation("Automatische Weiterleitung für $strOldAlias → $strNewAlias wurde erstellt");

			// Check if the page expects auto_items (redirect them too)
			if($objPage->requireItem){

				// Generate the auto_item aliases
				if($strSuffix != ''){
					$strOldAlias = str_replace($strSuffix, '/{auto_item}' . $strSuffix, $strOldAlias);
					$strNewAlias = str_replace($strSuffix, '/{auto_item}' . $strSuffix, $strNewAlias);
				} else {
					$strOldAlias = $strOldAlias . '/{auto_item}';
					$strNewAlias = $strNewAlias . '/{auto_item}';
				}

				// Generate auto_item redirect
				$objRedirect = new Terminal42RedirectModel();
				$objRedirect->name = "Automatische Regel für Generierte Unterseiten von : " . $objPage->title;
				$objRedirect->type = "basic";
				$objRedirect->inactive = 0;
				$objRedirect->requestHosts = serialize(array($strHost));
				$objRedirect->requestPath = $strOldAlias;
				$objRedirect->responseCode = \Config::get('redirectType') ?: 301;
				$objRedirect->responseUri = '/' . $strNewAlias;
				$objRedirect->tstamp = time();
				$objRedirect->requestRequirements = 'a:0:{}';
				$objRedirect->generated_by_memo = 1;
				$objRedirect->page_id = $objPage->id;
				$objRedirect->save();

				//Post Message in Backend
				Message::addConfirmation("Automatische Weiterleitung für $strOldAlias → $strNewAlias wurde erstellt");
			}



		}

		// Save the list of Aliases (cleaned by loop) to DB
		$objPage->alias_old = implode(',', $arrOldAlias);
		$objPage->save();

		// Delete the routing cache (so the old redirects disappear)
		self::clearCache();
	}

	public function clearCache()
	{
		// Get the our Cache-Clear-Service and warm it up
		\System::getContainer()->get('memo.routingcache.manager')->clearRouteCache();
	}

	public function removeRedirectsByPage($objPage, $strAlias)
	{
		// Get Parameters for the redirect rules
		$strHost = $objPage->domain;
		$strLanguage = $objPage->rootLanguage;
		$bolPrependLocale = \System::getContainer()->getParameter('prepend_locale');
		$strSuffix = \System::getContainer()->getParameter('contao.url_suffix');

		// Check if the old alias is the index alias (that disappears) and otherwise add the suffix
		if($strAlias == 'index'){
			$strAlias = '/';
		} else {
			$strAlias = '/' . $strAlias . $strSuffix;
		}

		// Check if a language should be added
		if($bolPrependLocale){
			$strAlias = $strLanguage . $strAlias;
		}

		$colAllRedirect = Terminal42RedirectModel::findBy(array('requestPath=?'), array($strAlias));

		// Check if there where any hits - if so, delete them
		if($colAllRedirect){
			$intCounter = 0;
			foreach($colAllRedirect as $objAllRedirect){
				$objAllRedirect->delete();
				$intCounter++;
			}

			if($intCounter == 1){
				Message::addInfo("Eine alte Weiterleitung auf die URL $strAlias wurde gelöscht - sonst wäre die Seite nicht erreichbar");
			} elseif($intCounter > 1){
				Message::addInfo("$intCounter Weiterleitungen auf die URL $strAlias wurden gelöscht - sonst wäre die Seite nicht erreichbar");
			}
		}
	}
}
