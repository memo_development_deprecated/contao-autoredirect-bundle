<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\AutoRedirectBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\PageRegular;
use Contao\LayoutModel;
use Contao\PageModel;

/**
 * @Hook("generatePage")
 */
class GeneratePageListener
{
	public function __invoke(PageModel $pageModel, LayoutModel $layout, PageRegular $pageRegular): void
	{
		// Check if the page was opend by Googlebot
		if (stristr($_SERVER['HTTP_USER_AGENT'], 'googlebot')){

			$objPage = PageModel::findById($pageModel->id);
			$objPage->last_crawl = time();
			$objPage->crawled = 1;
			$objPage->save();
		}
	}
}
