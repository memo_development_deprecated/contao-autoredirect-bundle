<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\AutoRedirectBundle\Model;
use Contao\Model;

/**
 * Class Terminal42RedirectModel
 *
 * Reads and writes Terminal42RedirectModel.
 */
class Terminal42RedirectModel extends Model
{
	/**
	 * Table name
	 * @var string
	**/
	protected static $strTable = 'tl_url_rewrite';

}
