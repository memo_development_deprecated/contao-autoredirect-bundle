<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_settings']['memoAutoRedirect'] = 'Automatische Weiterleitungen';

$GLOBALS['TL_LANG']['tl_settings']['redirectMode'] = array( 'Weiterleitungs-Modus', 'Wann soll automatisch weitergeleitet werden?' );
$GLOBALS['TL_LANG']['tl_settings']['redirectType'] = array( 'Weiterleitungs-Typ ', 'Wie soll weitergeleitet werden?' );

$GLOBALS['TL_LANG']['tl_settings']['redirectModes']['all'] = 'Alle Alias Änderungen weiterleiten';
$GLOBALS['TL_LANG']['tl_settings']['redirectModes']['none'] = 'Keine automatische Weiterleitungen - nur manuell eingepflegte alte Alias berücksichtigen';
$GLOBALS['TL_LANG']['tl_settings']['redirectModes']['crawled'] = 'Nur Alias Änderungen von gecrawlte Seiten weiterleiten';

$GLOBALS['TL_LANG']['tl_settings']['redirectTypes']['301'] = '301 - Permanent';
$GLOBALS['TL_LANG']['tl_settings']['redirectTypes']['302'] = '302 - Temporär';
$GLOBALS['TL_LANG']['tl_settings']['redirectTypes']['303'] = '303 - Andere';
