<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Fields
 */

$GLOBALS['TL_LANG']['tl_url_rewrite']['page_id'] = array("Seite", "Zugehörige Seite, wenn automatisch generiert.");
$GLOBALS['TL_LANG']['tl_url_rewrite']['generated_by_memo'] = array("Generierte Regel", "Wurde diese Regel automatisch vom AutoRedirectBundle generiert?");
