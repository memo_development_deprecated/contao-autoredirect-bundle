<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_page']['crawled'] = array( 'Von Google gecrawlt?', 'Wurde diese Seite von Google schon mal gesichtet? Erst wenn die Seite von Google gesichtet wurde, ist der AutoRedirect nötig' );
$GLOBALS['TL_LANG']['tl_page']['last_crawl'] = array( 'Letzter Crawl', 'Wann hat Google diese Seite zuletzt gecrawlt?' );
$GLOBALS['TL_LANG']['tl_page']['alias_old'] = array( 'Weitergeleitete Seitenalias', 'Falls sich der Seitenalias verändert, wird die Veränderung hier geloggt und darauf basierend auf den neuen Alias weitergeleitet. Mehrere Alias kommasepariert angeben.' );
