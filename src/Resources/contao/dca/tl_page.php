<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
	->addField('alias_old', 'type')
	->addField('crawled', 'alias_old')
	->addField('last_crawl', 'crawled')

	->applyToPalette('regular', 'tl_page')
;

$GLOBALS['TL_DCA']['tl_page']['fields']['crawled'] = [
	'label'                   => &$GLOBALS['TL_LANG']['tl_form_field']['crawled'],
	'exclude'                 => true,
	'filter'                  => true,
	'sorting'                 => false,
	'inputType'               => 'checkbox',
	'eval'                    => array('tl_class'=>'m12 w50 clr', 'doNotCopy'=>true),
	'sql'                     => ['type' => 'string', 'length' => 1, 'fixed' => true, 'default' => '']
];

$GLOBALS['TL_DCA']['tl_page']['fields']['alias_old'] = [
	'label'                   => &$GLOBALS['TL_LANG']['tl_form_field']['alias_old'],
	'exclude'                 => true,
	'filter'                  => true,
	'sorting'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('tl_class'=>'long clr', 'doNotCopy'=>true),
	'sql'                     => ['type' => 'text', 'notnull' => false, 'default' => '']
];

$GLOBALS['TL_DCA']['tl_page']['fields']['last_crawl'] = [
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['last_crawl'],
	'exclude'                 => true,
	'filter'                  => false,
	'sorting'                 => true,
	'flag'                    => 8,
	'inputType'               => 'text',
	'eval'                    => array('rgxp'=>'datim', 'mandatory'=>false, 'doNotCopy'=>true, 'tl_class'=>'w50 wizard', 'readonly'=>true, 'doNotCopy'=>true),
	'load_callback' => array
	(
		array('tl_page_redirect', 'loadDate')
	),
	'sql'                     => ['type' => 'integer', 'length' => 10, 'fixed' => true, 'notnull' => false]
];


class tl_page_redirect extends Backend
{

	public function loadDate($value)
	{

		if ($value && $value > 0) {
			return strtotime(date('d.m.Y H:i:s', $value));
		} else {
			return "";
		}
	}

}
