<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
->addLegend('memoAutoRedirect', 'chmod_legend', PaletteManipulator::POSITION_BEFORE)
->addField('redirectMode', 'memoAutoRedirect', PaletteManipulator::POSITION_APPEND)
->addField('redirectType', 'memoAutoRedirect', PaletteManipulator::POSITION_APPEND)
->applyToPalette('default', 'tl_settings');


$GLOBALS['TL_DCA']['tl_settings']['fields']['redirectMode'] = [
	'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['redirectMode'],
	'exclude'                 => true,
	'default'                 => 'crawled',
	'inputType'               => 'select',
	'options'                 => array('crawled', 'all', 'none'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_settings']['redirectModes'],
	'eval'                    => array('includeBlankOption'=>false, 'feGroup'=>'personal', 'tl_class'=>'w50'),
];

$GLOBALS['TL_DCA']['tl_settings']['fields']['redirectType'] = [
	'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['redirectType'],
	'exclude'                 => true,
	'default'                 => '301',
	'inputType'               => 'select',
	'options'                 => array('301', '302', '303'),
	'reference'               => &$GLOBALS['TL_LANG']['tl_settings']['redirectTypes'],
	'eval'                    => array('includeBlankOption'=>false, 'feGroup'=>'personal', 'tl_class'=>'w50'),
];

