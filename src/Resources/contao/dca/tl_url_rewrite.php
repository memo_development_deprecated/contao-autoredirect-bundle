<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
	->addField('generated_by_memo', 'inactive')
	->addField('page_id', 'generated_by_memo')
	->applyToPalette('default', 'tl_url_rewrite');
PaletteManipulator::create()
	->addField('generated_by_memo', 'inactive')
	->addField('page_id', 'generated_by_memo')
	->applyToPalette('basic', 'tl_url_rewrite');
PaletteManipulator::create()
	->addField('generated_by_memo', 'inactive')
	->addField('page_id', 'generated_by_memo')
	->applyToPalette('expert', 'tl_url_rewrite');

$GLOBALS['TL_DCA']['tl_url_rewrite']['fields']['generated_by_memo'] = array(
	'label' => &$GLOBALS['TL_LANG']['tl_url_rewrite']['generated_by_memo'],
	'exclude' => true,
	'filter' => true,
	'inputType' => 'checkbox',
	'eval' => ['tl_class' => 'clr w50'],
	'sql' => ['type' => 'boolean', 'default' => 0],
);

$GLOBALS['TL_DCA']['tl_url_rewrite']['fields']['page_id'] = array(
	'label' => &$GLOBALS['TL_LANG']['tl_url_rewrite']['page_id'],
	'exclude' => true,
	'filter' => true,
	'inputType' => 'pageTree',
	'eval' => ['tl_class' => 'w50'],
	'sql' => ['type' => 'integer', 'fixed'=>true, 'length'=>10, 'default' => 0],
);

