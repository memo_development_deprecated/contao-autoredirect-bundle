<?php
/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\AutoRedirectBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Memo\AutoRedirectBundle\AutoRedirectBundle;
use Terminal42\UrlRewriteBundle\Terminal42UrlRewriteBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
		return [
			BundleConfig::create(AutoRedirectBundle::class)->setLoadAfter([ContaoCoreBundle::class, Terminal42UrlRewriteBundle::class])
		];
    }
}
