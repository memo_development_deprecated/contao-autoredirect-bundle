<?php

/**
 * @package   AutoRedirectBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\AutoRedirectBundle\Services;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\File;

class ClearRouteCacheService
{

	private $fs;

	private $router;

	private $cacheDir;

	public function __construct(Filesystem $fs, $router, $kernelCacheDir)
	{
		$this->fs       = $fs;
		$this->router   = $router;
		$this->cacheDir = $kernelCacheDir;
	}

	public function clearRouteCache()
	{
		// Delete all routing-files
		$finder = new Finder();
		foreach ($finder->files()->depth('== 0')->in($this->cacheDir) as $file) {
			if (preg_match('/routes|Url/', $file->getFilename()) === 1) {
				$this->fs->remove($file->getRealPath());
			}
		}

		// Warm up the cache again
		$this->router->warmUp($this->cacheDir);
	}
}
