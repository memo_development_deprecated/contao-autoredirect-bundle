# Contao Auto Redirect Bundle

## About
This bundle detects if a page has been seen by google yet (detects the user-agent).
If the page has been crawled and then a pagealias is changed, all changes (no limit on age) will be tracked and redirected.
So if a page was called our-services.html, and is changed to services.html, a redirect (301)  is created for the old alias.
This bundle handles the following cases:

- **Domains** Multiple pages in the same contao-installation are handles by adding the defined domain to the redirects
- **Languages** Multiple languages are also handled (because a page alias does not contain the language yet), so if you have prepend_locale on true, this bundle still works.
- **History** If you change multiple-times, all the past redirects are updated to the newest alias automatically
- **Loops** If you change back to a old version of the alias, that redirect will be deleted, to prevent a loop
- **Magic index alias** If you use the index-alias trick for a clean landingpage, this bundle will respect that when redirecting
- **Manual usage** You can disable the automatic generation of new redirects and just use the field for "old aliased" to create redirects to the current page in a very simple way - like synonyms

## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-autoredirect-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-autoredirect-bundle": "^1.0",
```

## Usage (German)
Once installed, you can define how the bundle should work in the contao settings.

### Weiterleitungs-Modus
**Nur Alias Änderungen von gecrawlten Seiten weiterleiten**

Beim Aufruf jeder Seite wird via dem Useragent erkannt, ob es sich um den Google-Bot handelt.
Sofern die Seite irgendwann mal von Google erfasst wurde, erkennt man dies an der Checkbox "Von Google gecrawlt?".
Sofern diese angewählt ist (manuell oder durch diesen Automatismus) wird bei einer Seitenalias-Anpassung automatisch erkannt, dass eine Weiterleitung nötig ist.

**Alle Alias Änderungen weiterleiten**

Unabhängig vom Crawler-Status werden alle Anpassungen am Seitenalias nachverfolgt und weitergeleitet.

**Keine automatische Weiterleitung**

Es werden keine automatischen Weiterleitungen bei der Anpassung des Seitenaliases generiert.
Manuell hinzugefügte "Weitergeleitete Seitenalias" werden aber immer noch berücksichtigt.

### Weiterleitungs-Typ
Gängie Weiterleitungstypen (301, 302, 303).
Sprich dies definiert, ob eine Weiterleitung nur temporär (für Google nicht interessant) oder permanent (für Ranking Übertragung an neue Seite nötig) ist.

## Screenshots

### Contao Einstellungen
![Contao Backend Settings Preview](backend-settings.png)

### Contao Seitenstruktur
![Contao Backend Page Preview](backend-page.png)

## Contribution
Bug reports and pull requests are welcome
